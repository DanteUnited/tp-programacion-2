# Condición

Completar informe y corregir implementacion.

# Observaciones

Las clases se deben relacion por Objetos y no por ID.

# Corregir

## Diagrama

- [ ] ¿Qué es Keygen?
- [ ] Servicio TIENE Cliente y Especialista. Tiene el objeto, no los IDs.

## Irep

- [ ] Falta cruzar datos.

## Conceptos

- [ ] Mal explicado sobrecarga.

## String builder

- [ ] Mal usado.

## toString

- [ ] Muestra mucha info, siendo poco legible.

## equals

- [ ] carece de equals.

## Calidad del código

- [ ] Las relaciones entre clases se deben hacer con Objetos.
- [ ] Observación: Repite mucho código y abusa de la redundancia de datos.

