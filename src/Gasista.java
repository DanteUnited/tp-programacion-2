package src;

public abstract class Gasista extends Servicio {
    private Integer cantArtefactos;
    private double precioPorArtefacto;

    public Gasista(Integer dniCliente, Integer nroEspecialista, String domicilio, Integer cantArtefactos, double precioPorArtefacto) {
        super(dniCliente, nroEspecialista, domicilio, "");

        if (cantArtefactos < 0)
            throw new RuntimeException("Error. La cantidad de artefactos no pueden ser menor que 0.");
        if (precioPorArtefacto < 0)
            throw new RuntimeException("Error. El precio por artefactos no puede ser menor que 0.");

        this.cantArtefactos = cantArtefactos;
        this.precioPorArtefacto = precioPorArtefacto;
    }
    public Integer getCantArtefactos() {
        return cantArtefactos;
    }

    public double getPrecioPorArtefacto() {
        return precioPorArtefacto;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tCantidad de artefactos: " + this.cantArtefactos + "\n");
        sb.append("\tPrecio por artefactos: " + this.precioPorArtefacto + "\n");

        return super.toString() + sb.toString();
    }
}
