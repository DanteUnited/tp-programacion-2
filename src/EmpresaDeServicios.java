package src;

import java.util.*;


public class EmpresaDeServicios {
    private Map<Integer, Servicio> servicios;
    private Map<Integer, Cliente> clientes;
    private Map<Integer, Especialista> especialistas;

    private double gastosElectricidad;
    private double gastosGasistaInstalacion;
    private double gastosGasistaRevision;
    private double gastosPintura;
    private double gastosPinturaEnAltura;

    private Integer servRealizadosElec;
    private Integer servRealizadosGasInstalacion;
    private Integer servRealizadosGasRevision;
    private Integer servRealizadosPin;
    private Integer servRealizadosPinEnAltura;

    public EmpresaDeServicios() {
        servicios = new HashMap<>();
        clientes = new HashMap<>();
        especialistas = new HashMap<>();

        gastosElectricidad = 0;
        gastosGasistaInstalacion = 0;
        gastosGasistaRevision = 0;
        gastosPintura = 0;
        gastosPinturaEnAltura = 0;

        servRealizadosElec = 0;
        servRealizadosGasInstalacion = 0;
        servRealizadosGasRevision = 0;
        servRealizadosPin = 0;
        servRealizadosPinEnAltura = 0;
    }

    /**
     * Registra un nuevo cliente en el sistema dado su:
     * - dni,
     * - nombre y
     * - teléfono de contacto.
     * Si el dni ya está en el sistema se debe generar una
     * excepción.
     */
    public void registrarCliente(Integer dni, String nombre, String telefono) {
        if (clientes.containsKey(dni)) throw new RuntimeException("Error. Cliente ya se encuentra registrado.");
        else {
            Cliente cliente = new Cliente(nombre, dni, telefono);
            clientes.put(dni, cliente);
        }
    }

    /**
     * Registra un nuevo especialista en el sistema dados su:
     * - nroEspecialista,
     * - nombre,
     * - teléfono y
     * - tipo De servicio en el que se especializa.
     * Si el nroEspecialista ya está registrado en el sistema
     * se debe generar una excepción.
     */
    public void registrarEspecialista(Integer identificacion, String nombre, String telefono, String especialidad) {
        boolean especialidadExiste = especialidad.equals("GasistaInstalacion") || especialidad.equals("GasistaRevision") || especialidad.equals("Electricidad") || especialidad.equals("Pintura") || especialidad.equals("PinturaEnAltura");
        if (!especialidadExiste) throw new RuntimeException("Error. Tipo de especialidad no existe");

        if (especialistas.containsKey(identificacion))
            throw new RuntimeException("Error. Especialista ya se encuentra registrado.");
        else {
            Especialista especialista = new Especialista(nombre, identificacion, telefono, especialidad);
            especialistas.put(identificacion, especialista);
        }
    }

    /**
     * Se registra la prestación de un servicio de tipo Electricidad en el sistema
     * ingresando los datos necesarios para solicitar un servicio y además:
     * - precio por hora de trabajo del especialista
     * - cantidad de horas estimadas que llevará realizar el trabajo.
     * Se devuelve el código único del servicio a realizar.
     * Si el dni o el nroEspecialista no están registrados en el sistema se debe
     * generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * una excepción.
     * Si el precio por hora o las horas de trabajo estimado son menores o
     * iguales a 0, se debe generar una excepción.
     */
    public Integer solicitarServicioElectricidad(int dniCliente, int nroEspecialista, String direccion, double precioPorHora, int horasDeTrabajo) {
        comprobarClienteEspecialista(dniCliente, nroEspecialista);

        Especialista especialista = especialistas.get(nroEspecialista);
        if (!especialista.getEspecialidad().equals("Electricidad")) {
            throw new RuntimeException("Error. El trabajador no se especializa en esta area.");
        }
        if (horasDeTrabajo <= 0 || precioPorHora <= 0) {
            throw new RuntimeException("Error. Las horas de trabajo no pueden ser menores a 0.");
        }
        Electricidad electricista = new Electricidad(dniCliente, nroEspecialista, direccion, precioPorHora, horasDeTrabajo);
        servicios.put(electricista.codigoDeServicio, electricista);
        return electricista.codigoDeServicio;
    }

    /**
     * Se registra la prestación de un servicio de tipo Pintura en el sistema
     * ingresando los datos necesarios para solicitar un servicio y además:
     * - precio por pintar un metro cuadrado
     * - cantidad de metros cuadrados a pintar.
     * Se devuelve el código único del servicio a realizar.
     * Si el dni o el nroEspecialista no están registrados en el sistema se debe
     * generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * una excepción.
     * Si el precio por metro cuadrado o los metros cuadrados son menores o
     * iguales a 0, se debe generar una excepción.
     * 5 de 7
     */
    public Integer solicitarServicioPintura(Integer dniCliente, int nroEspecialista, String direccion, int metrosCuadrados, Integer precioPorMetroCuadrado) {
        comprobarClienteEspecialista(dniCliente, nroEspecialista);

        Especialista especialista = especialistas.get(nroEspecialista);
        if (!especialista.getEspecialidad().equals("Pintura")) {
            throw new RuntimeException("Error. El trabajador no se especializa en esta area.");
        }
        if (metrosCuadrados <= 0 || precioPorMetroCuadrado <= 0) {
            throw new RuntimeException("Error. Los metros cuadrados o el precio son inválidos.");
        }
        Pintura pintura = new Pintura(dniCliente, nroEspecialista, direccion, metrosCuadrados, precioPorMetroCuadrado);
        servicios.put(pintura.codigoDeServicio, pintura);
        return pintura.codigoDeServicio;
    }

    /**
     * Se registra la prestación de un servicio de tipo PinturaEnAltura en el
     * sistema ingresando los datos necesarios para solicitar un servicio y además:
     * - precio por pintar un metro cuadrado
     * - cantidad de metros cuadrados a pintar
     * - nro de piso en el que se realizará el trabajo.
     * - costo del seguro
     * - costo del alquiler de los andamios.
     * Se devuelve el código único del servicio a realizar.
     * Si el dni o el nroEspecialista no están registrados en el sistema se debe
     * generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * una excepción.
     * Si el precio por metro cuadrado o los metros cuadrados o el piso o el costo
     * del seguro o el costo del alquiler de los andamios son menores o iguales a 0,
     * se debe generar una excepción.
     */
    public Integer solicitarServicioPintura(int dniCliente, int nroEspecialista, String direccion, int metrosCuadrados, double precioPorMetroCuadrado, Integer pisoATrabajar, double seguro, double alqAndamios) {
        comprobarClienteEspecialista(dniCliente, nroEspecialista);

        Especialista especialista = especialistas.get(nroEspecialista);
        if (!especialista.getEspecialidad().equals("PinturaEnAltura")) {
            throw new RuntimeException("Error. El trabajador no se especializa en esta area.");
        }
        if (metrosCuadrados <= 0 || precioPorMetroCuadrado <= 0 || pisoATrabajar <= 0 || seguro <= 0 || alqAndamios <= 0) {
            throw new RuntimeException("Error. Los metro cuadrados o el precio o el piso a trabajar o el costo del seguro o el costo de los andamios son inválidos.");
        }
        PinturaEnAltura pintura = new PinturaEnAltura(dniCliente, nroEspecialista, direccion, metrosCuadrados, precioPorMetroCuadrado, pisoATrabajar, seguro, alqAndamios);
        servicios.put(pintura.codigoDeServicio, pintura);
        return pintura.codigoDeServicio;
    }

    /**
     * Se registra la prestación de un servicio de tipo GasistaInstalacion en el
     * sistema ingresando los datos necesarios para solicitar un servicio y además:
     * - cantidad de artefactos a instalar
     * - precio por la instalación de un artefacto.
     * Se devuelve el código único del servicio a realizar.
     * Si el dni o el nroEspecialista no están registrados en el sistema se debe
     * generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * una excepción.
     * Si el precio de instalación o la cantidad de artefactos a instalar son
     * menores o iguales a 0, se debe generar una excepción.
     */
    public Integer solicitarServicioGasistaInstalacion(int dniCliente, int nroEspecialista, String direccion, int cantArtefactos, double precioPorArtefacto) {
        comprobarClienteEspecialista(dniCliente, nroEspecialista);

        Especialista especialista = especialistas.get(nroEspecialista);
        if (!Objects.equals(especialista.getEspecialidad(), "GasistaInstalacion")) {
            throw new RuntimeException("Error. El trabajador no se especializa en esta area.");
        }
        if (cantArtefactos <= 0 || precioPorArtefacto <= 0) {
            throw new RuntimeException("Error");
        }
        GasistaInstalacion gasista = new GasistaInstalacion(dniCliente, nroEspecialista, direccion, cantArtefactos, precioPorArtefacto);
        servicios.put(gasista.codigoDeServicio, gasista);
        return gasista.codigoDeServicio;

    }


    /**
     * Se registra la prestación de un servicio de tipo GasistaRevison en el
     * sistema ingresando los datos necesarios para solicitar un servicio y además:
     * - cantidad de artefactos a revisar
     * - precio por la revisión de un artefacto.
     * Se devuelve el código único del servicio a realizar.
     * Si el dni o el nroEspecialista no están registrados en el sistema se debe
     * generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * 6 de 7
     * una excepción.
     * Si el precio de instalación o la cantidad de artefactos a revisar son
     * menores o iguales a 0, se debe generar una excepción.
     */
    public Integer solicitarServicioGasistaRevision(int dniCliente, int nroEspecialista, String direccion, int cantArtefactos, double precioPorArtefacto) {
        comprobarClienteEspecialista(dniCliente, nroEspecialista);

        Especialista especialista = especialistas.get(nroEspecialista);
        if (!especialista.getEspecialidad().equals("GasistaRevision")) {
            throw new RuntimeException("Error. El trabajador no se especializa en esta area.");
        }
        if (cantArtefactos <= 0 || precioPorArtefacto <= 0) {
            throw new RuntimeException("Error. La cantidad de artefactos o el precio son inválidos.");
        }
        GasistaRevision gasista = new GasistaRevision(dniCliente, nroEspecialista, direccion, cantArtefactos, precioPorArtefacto);
        servicios.put(gasista.codigoDeServicio, gasista);
        return gasista.codigoDeServicio;
    }

    /**
     * Se registra que el servicio solicitado ya fué concluido. Para esto se debe
     * ingresar el costo de los materiales utilizados para poder realizar el
     * trabajo.
     * Se devuelve el precio que se debe facturar al cliente.
     * Este precio se obtiene sumando el costo de los materiales con el costo del
     * servicio realizado. Cada tipo de servicio tiene su forma de calcular el
     * costo del trabajo.
     * Si el código de servicio no está en el sistema o el mismo ya fué finalizado,
     * se debe generar una excepción.
     * Se debe realizar esta operación en O(1).
     */
    public double finalizarServicio(int codServicio, double costoMateriales) {

        if (!servicios.containsKey(codServicio)) {
            throw new RuntimeException("Error. El Servicio no se encuentra en la base de datos.");
        }

        Servicio servicio = servicios.get(codServicio);
        double gastos = servicio.calcularGastos(costoMateriales);

        if (servicio.getServTerminado()) {
            throw new RuntimeException("Error. El Servicio ya no se encuentra activo.");
        }
        servicio.finalizarServicio();
        switch (servicio.getTipoDeServicio()) {
            case "GasistaInstalacion" -> {
                gastosGasistaInstalacion += gastos;
                servRealizadosGasInstalacion += 1;
            }
            case "GasistaRevision" -> {
                gastosGasistaRevision += gastos;
                servRealizadosGasRevision += 1;
            }
            case "Electricidad" -> {
                gastosElectricidad += gastos;
                servRealizadosElec += 1;
            }
            case "Pintura" -> {
                gastosPintura += gastos;
                servRealizadosPin += 1;
            }
            case "PinturaEnAltura" -> {
                gastosPinturaEnAltura += gastos;
                servRealizadosPinEnAltura += 1;
            }
        }
        return gastos;
    }

    /**
     * Devuelve un diccionario cuya clave es el tipo de servicio y el valor es la
     * cantidad de servicios realizados de ese tipo.
     * Si un tipo de servicio no tuvo ninguna demanda, el valor de esa entrada debe
     * ser 0.
     */
    public Map<String, Integer> cantidadDeServiciosRealizadosPorTipo() {
        Map<String, Integer> serviciosRealizados = new HashMap<>();

        serviciosRealizados.put("Electricidad", servRealizadosElec);
        serviciosRealizados.put("GasistaInstalacion", servRealizadosGasInstalacion);
        serviciosRealizados.put("GasistaRevision", servRealizadosGasRevision);
        serviciosRealizados.put("Pintura", servRealizadosPin);
        serviciosRealizados.put("PinturaEnAltura", servRealizadosPinEnAltura);

        return serviciosRealizados;
    }

    /**
     * Devuelve la suma del precio facturado de todos los servicios finalizados que
     * son del tipo pasado por parámetro.
     * Si el tipo de servicio es invalido, debe generar una excepción.
     * Se debe realizar esta operación en O(1).
     */
    public double facturacionTotalPorTipo(String tipoDeServicio) {
        switch (tipoDeServicio) {
            case "GasistaInstalacion":
                return gastosGasistaInstalacion;
            case "GasistaRevision":
                return gastosGasistaRevision;
            case "Electricidad":
                return gastosElectricidad;
            case "Pintura":
                return gastosPintura;
            case "PinturaEnAltura":
                return gastosPinturaEnAltura;
            default:
                throw new RuntimeException("Error. El tipo de servicio es inválido.");
        }
    }

    /**
     * Devuelve la suma del precio facturado de todos los servicios finalizados que
     * realizó la empresa.
     */
    public double facturacionTotal() {
        return gastosPintura + gastosPinturaEnAltura + gastosGasistaInstalacion + gastosGasistaRevision + gastosElectricidad;
    }

    /**
     * Debe cambiar el especialista responsable del servicio.
     * Si código de Servicio o el nroEspecialista no están registrados en el sistema
     * se debe generar una excepción.
     * Si el especialista no se especializa en este tipo de servicio se debe generar
     * una excepción.
     * Se debe realizar esta operación en O(1).
     */
    public void cambiarResponsable(int codServicio, int nroEspecialista) {
        if (!servicios.containsKey(codServicio))
            throw new RuntimeException("Error. Código de servicio no está registrado.");
        if (!especialistas.containsKey(nroEspecialista))
            throw new RuntimeException("Error. Número de especialista no está registrado.");

        String tipoDeServicio = servicios.get(codServicio).getTipoDeServicio();
        String servicioEspecialista = especialistas.get(nroEspecialista).getEspecialidad();

        if (!tipoDeServicio.equals(servicioEspecialista))
            throw new RuntimeException("Error. El especialista no se especializa en ese servicio.");

        servicios.get(codServicio).cambiarEspecialista(nroEspecialista);
    }

    /**
     * Devuelve un String con forma de listado donde cada renglón representa un
     * servicio realizado.
     * Cada renglón debe respetar el siguiente formato:
     * " + [ codServicio - GasistaInstalacion ] Dirección"
     * por ejemplo:
     * " + [ 1492 - GasistaInstalacion ] uan María Gutiérrez 1150"
     * Si el nroEspecialista no está registrado en el sistema, se debe generar una
     * excepción.
     * Si el especialista no ha realizado ningún servicio hasta el momento, se debe
     * devolver un String vacío.
     */
    public String listadoServiciosAtendidosPorEspecialista(Integer nroEspecialista) {
        if (!especialistas.containsKey(nroEspecialista))
            throw new RuntimeException("Error. El especialista no está registrado en el sistema.");

        final StringBuilder sb = new StringBuilder();

        Servicio servicioActual;
        for (Map.Entry<Integer, Servicio> entry : servicios.entrySet()) {
            servicioActual = entry.getValue();

            if (servicioActual.getServTerminado() && servicioActual.getNroEspecialista().equals(nroEspecialista))
                sb.append(" + [ " + servicioActual.codigoDeServicio + " - " + servicioActual.tipoDeServicio + " ] "
                        + servicioActual.getDomicilio() + "\n");
        }

        return sb.toString();
    }

    private void comprobarClienteEspecialista(Integer dniCliente, Integer nroEspecialista) {
        if (!clientes.containsKey(dniCliente)) {
            throw new RuntimeException("Error. El cliente no se encuentra registrado en la base de datos.");
        }
        if (!especialistas.containsKey(nroEspecialista)) {
            throw new RuntimeException("Error. El trabajador no se encuentra registrado en la base de datos.");
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        Servicio servicioActual;
        Persona personaActual;

        sb.append("Información de servicios:\n");
        for (Map.Entry<Integer, Servicio> entry : servicios.entrySet()) {
            sb.append(entry.getValue().toString() + "\n");
        }

        sb.append("Información de clientes:\n");
        for (Map.Entry<Integer, Cliente> entry : clientes.entrySet()) {
            sb.append(entry.getValue().toString() + "\n");
        }

        sb.append("Información de especialistas:\n");
        for (Map.Entry<Integer, Especialista> entry : especialistas.entrySet()) {
            sb.append(entry.getValue().toString() + "\n");
        }

        return sb.toString();
    }
}

