package src;

public class GasistaRevision extends Gasista {

    public GasistaRevision(Integer dniCliente, Integer nroEspecialista, String domicilio, Integer cantArtefactos, double precioPorArtefacto) {
        super(dniCliente, nroEspecialista, domicilio, cantArtefactos, precioPorArtefacto);
        this.tipoDeServicio = "GasistaRevision";
        this.codigoDeServicio = KeyGen.keyGen();
    }

    @Override
    public double calcularGastos(double CostoMateriales) {
        CostoMateriales += getCantArtefactos() * getPrecioPorArtefacto();
        if (getCantArtefactos() > 5){
            CostoMateriales *= 0.9;
        }
        if (getCantArtefactos() > 10){
            CostoMateriales *= 0.85;
        }
        return CostoMateriales;
    }
}
