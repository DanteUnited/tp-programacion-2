package src;

public class GasistaInstalacion extends Gasista {

    public GasistaInstalacion(Integer dniCliente, Integer nroEspecialista, String domicilio, Integer cantArtefactos, double precioPorArtefacto) {
        super(dniCliente, nroEspecialista, domicilio, cantArtefactos, precioPorArtefacto);
        this.tipoDeServicio = "GasistaInstalacion";
        this.codigoDeServicio = KeyGen.keyGen();
    }

    @Override
    public double calcularGastos(double CostoMateriales) {
        return CostoMateriales + getCantArtefactos() * getPrecioPorArtefacto();
    }
}
