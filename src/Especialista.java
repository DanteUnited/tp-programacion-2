package src;

public class Especialista extends Persona {
    private String especialidad;

    public Especialista(String nombre, Integer identificacion, String telefono, String especialidad) {
        super(nombre, identificacion, telefono);
        if (especialidad.length() == 0)
            throw new RuntimeException("Error. Especialidad inválida.");
        this.especialidad = especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tEspecialidad: " + this.especialidad + "\n");

        return super.toString() + sb.toString();
    }
}
