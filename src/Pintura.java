package src;

public class Pintura extends Servicio {

    private Integer metrosCuadrados;
    private double precioPorMetrosCuadrados;

    public Pintura(Integer dniCliente, Integer nroEspecialista, String domicilio, Integer metrosCuadrados, double precioPorMetrosCuadrados) {
        super(dniCliente, nroEspecialista, domicilio, "Pintura");

        if (metrosCuadrados <= 0)
            throw new RuntimeException("Error. Los metros cuadrados no pueden ser menor o igual que 0");
        if (precioPorMetrosCuadrados <= 0)
            throw new RuntimeException("Error. El precio por hora no puede ser menor o igual que 0");

        this.metrosCuadrados = metrosCuadrados;
        this.precioPorMetrosCuadrados = precioPorMetrosCuadrados;
        this.codigoDeServicio = KeyGen.keyGen();
    }

    public double calcularGastos(double CostoMateriales) {
        return CostoMateriales + precioPorMetrosCuadrados * metrosCuadrados;
    }

    public Integer getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public double getPrecioPorMetrosCuadrados() {
        return precioPorMetrosCuadrados;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tCantidad de metros cuadrados: " + this.metrosCuadrados + "\n");
        sb.append("\tPrecio por metros cuadrados: " + this.precioPorMetrosCuadrados + "\n");

        return super.toString() + sb.toString();
    }
}
