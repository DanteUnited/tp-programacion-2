package src;

public class PinturaEnAltura extends Pintura {

    private Integer pisoATrabajar;
    private double seguro;
    private double alqAndamios;

    public PinturaEnAltura(Integer dniCliente, Integer nroEspecialista, String domicilio, Integer metrosCuadrados, double precioPorMetrosCuadrados, Integer pisoATrabajar, double seguro, double alqAndamios) {
        super(dniCliente, nroEspecialista, domicilio, metrosCuadrados, precioPorMetrosCuadrados);
        this.tipoDeServicio = "PinturaEnAltura";

        if (pisoATrabajar <= 0)
            throw new RuntimeException("Error. El piso a trabajar debe ser mayor que 0.");
        if (seguro <= 0)
            throw new RuntimeException("Error. El precio del seguro debe ser mayor que 0.");
        if (alqAndamios <= 0)
            throw new RuntimeException("Error. El precio de los andamios debe ser mayor que 0.");

        this.pisoATrabajar = pisoATrabajar;
        this.seguro = seguro;
        this.alqAndamios = alqAndamios;
        this.codigoDeServicio = KeyGen.keyGen();
    }

    public double calcularGastos(double CostoMateriales) {
        CostoMateriales += getPrecioPorMetrosCuadrados() * getMetrosCuadrados() + seguro;
        if (pisoATrabajar >= 5) {
            CostoMateriales += (alqAndamios * 1.2);
        } else {
            CostoMateriales += alqAndamios;
        }
        return CostoMateriales;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tPiso a trabajar: " + this.pisoATrabajar + "\n");
        sb.append("\tCosto del seguro: " + this.seguro + "\n");
        sb.append("\tCosto del alquiler de andamios: " + this.alqAndamios + "\n");

        return super.toString() + sb.toString();
    }

}