package src;

public abstract class Servicio {
    protected String tipoDeServicio;
    private Integer dniCliente;
    private Integer nroEspecialista;
    private String domicilio;
    protected Integer codigoDeServicio;
    private boolean servTerminado;

    public Servicio(Integer dniCliente, Integer nroEspecialista, String domicilio, String tipoDeServicio) {
        if (dniCliente <= 0)
            throw new RuntimeException("Error. El DNI no puede ser menor o igual que 0");
        if (nroEspecialista <= 0)
            throw new RuntimeException("Error. El número de especialista no puede ser menor o igual que 0");
        if (domicilio.length() == 0)
            throw new RuntimeException("Error. El domicilio no puede estar vacío.");

        this.tipoDeServicio = tipoDeServicio;
        this.dniCliente = dniCliente;
        this.nroEspecialista = nroEspecialista;
        this.domicilio = domicilio;
    }

    public void finalizarServicio() {
        servTerminado = true;
    }

    public abstract double calcularGastos(double costoMateriales);

    public void cambiarEspecialista(Integer nroEspecialista) {
        this.nroEspecialista = nroEspecialista;
    }

    public String getTipoDeServicio() {
        return tipoDeServicio;
    }

    public Integer getNroEspecialista() {
        return nroEspecialista;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public boolean getServTerminado() {
        return servTerminado;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tTipo de servicio: " + this.tipoDeServicio + "\n");
        sb.append("\tDNI cliente: " + this.dniCliente + "\n");
        sb.append("\tNúmero especialista: " + this.nroEspecialista + "\n");
        sb.append("\tDomicilio: " + this.domicilio + "\n");
        sb.append("\tCódigo de servicio: " + this.codigoDeServicio + "\n");
        sb.append("\tServicio está terminado: " + this.servTerminado + "\n");

        return sb.toString();
    }
}

