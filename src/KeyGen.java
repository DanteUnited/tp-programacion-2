package src;
import java.util.Random;
public class KeyGen {
    static String caracteres = "0123456789";
    static Random random = new Random();

    public static Integer keyGen() {
        StringBuilder codigo = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            char randomNum = caracteres.charAt(random.nextInt(caracteres.length()));
            codigo.append(randomNum);
        }
        Integer codigoInt = Integer.parseInt(String.valueOf(codigo));
        return codigoInt;
    }
}
