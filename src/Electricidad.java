package src;

public class Electricidad extends Servicio {

    private double horasDeTrabajo;
    private double valorPorHora;

    public Electricidad(Integer dniCliente, Integer nroEspecialista, String domicilio, double valorPorHora, double horasDeTrabajo) {
        super(dniCliente, nroEspecialista, domicilio, "Electricidad");

        if (horasDeTrabajo <= 0)
            throw new RuntimeException("Error. Las horas de trabajo no pueden ser menor o igual que 0");
        if (valorPorHora <= 0)
            throw new RuntimeException("Error. El precio por trabajo no pueden ser menor o igual que 0");

        this.horasDeTrabajo = horasDeTrabajo;
        this.valorPorHora = valorPorHora;
        this.codigoDeServicio = KeyGen.keyGen();
    }

    public double calcularGastos(double CostoMateriales) {
        return CostoMateriales + valorPorHora * horasDeTrabajo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tCantidad de horas trabajadas: " + this.horasDeTrabajo + "\n");
        sb.append("\tValor por hora trabajada: " + this.valorPorHora + "\n");

        return super.toString() + sb.toString();
    }
}