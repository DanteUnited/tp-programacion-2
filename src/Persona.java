package src;

public abstract class Persona {
    private String nombre;
    private Integer identificacion;
    private String telefono;


    Persona(String nombre, Integer identificacion, String telefono) {
        if (identificacion < 0)
            throw new RuntimeException("Error. DNI inválido.");
        if (nombre.length() == 0)
            throw new RuntimeException("Error. Nombre inválido.");
        if (telefono.length() == 0)
            throw new RuntimeException("Error. Teléfono inválido.");
        if (Integer.parseInt(telefono) < 99999999 && Integer.parseInt(telefono) > 1000000000)
            throw new RuntimeException("Error. Número de télefono inválido.");

        this.nombre = nombre;
        this.identificacion = identificacion;
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append("\tNombre: " + this.nombre + "\n");
        sb.append("\tIdentificación: " + this.identificacion + "\n");
        sb.append("\tTeléfono: " + this.telefono + "\n");

        return sb.toString();
    }
}
